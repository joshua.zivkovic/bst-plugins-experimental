===============================
bst-plugins-experimental 1.99.4
===============================
o bazelize: Do not include libraries that don't start with 'lib' to skip loadable modules
o bazelize: traverse stack elements when extracting dependencies
o bazelize: Extend src filter for cc_library to include soname with version infomation
o collect_manifest: Revert "Extend default regular expression for underscore/dash separators"
o collect_manifest: Augment manifests with alias and git_repo with refspecs

===============================
bst-plugins-experimental 1.99.3
===============================
o go_module: Support projects with multiple versioned sub-trees in git. Support
  larger subset of go.sum syntax
o {git_repo,go_module}: Fix MissingCommitError being raised in some cases

===============================
bst-plugins-experimental 1.99.2
===============================
o go_module: Fix repos to be staged into expected vendor directories.

===============================
bst-plugins-experimental 1.99.1
===============================
o collect_manifest: Consider also _ and - as valid separators for version by default
  and make logging less verbose.
o {git_repo,go_module}: Fix spurious unaliased-url warning when tracking
o patch_queue: Fix implementation of API for collect_manifest plugin

===============================
bst-plugins-experimental 1.99.0
===============================
o snap_image: Port to non-deprecated ruamel.yaml API
o flatpak_image: Make it possible to run appstream compose
o git_repo: don't specify depth in fallback codepath
o {pypi,cpan}: Use buildstream 2.2 API conditionally
o Add go_module plugin
o {git_repo,go_module}: Implement dulwich version check in preflight
o Define API for exporting information for collect_manifest plugin
o Add support for bearer HTTP authentication (pypi, cpan)

===============================
bst-plugins-experimental 1.98.0
===============================
o Improve documentation for git_repo
o More robust generation of git-describe format refs for git_repo
o Support git_repo sources in collect_manifest
o BREAKING CHANGE: Rewrite the bazelize plugin. See the documentation for
  features of the new version.

===============================
bst-plugins-experimental 1.97.1
===============================
o Bug fixes to release tooling

===============================
bst-plugins-experimental 1.97.0
===============================
o Project ported to pyproject. Wheels are now published to pypi
o Further fixes to check_forbidden error reporting

===============================
bst-plugins-experimental 1.96.1
===============================
o Improvements to check_forbidden output when elements are forbidden

===============================
bst-plugins-experimental 1.96.0
===============================
o BREAKING CHANGE: change the way url is specified in cpan
o add support for .netrc authentication to cpan and pypi

===============================
bst-plugins-experimental 1.95.11
===============================
o git_repo fixed to stage objects for symbolic links
o git_repo fixed regression introduced in 1.95.8 where all commits were marked as
  shallow for non-tag commits

===============================
bst-plugins-experimental 1.95.10
===============================
o git_repo fixed crash when creating workspaces

===============================
bst-plugins-experimental 1.95.9
===============================
o git_repo netrc fixed not to throw exception when no netrc present
o make_maker and modulebuild now call make pure_install instead of make install

===============================
bst-plugins-experimental 1.95.8
===============================
o git_repo performance improvements, .netrc support, explicit workspace support
  and documentation

===============================
bst-plugins-experimental 1.95.7
===============================
o git_repo plugin fetch performance improvements for staging
o oci plugin documents gzip option

===============================
bst-plugins-experimental 1.95.6
===============================
o New git_repo plugin that can track multiple refs in a repo using wildcards 
  and uses source cache more efficiently. It treats refs like version numbers
  when selecting latest matching ref.
  The new plugin is only available when consuming this project as a junction.
o Bug fix to git_module that path is part of cache key


===============================
bst-plugins-experimental 1.95.5
===============================
o git_module no longer depends on previous source on fetching, only on tracking
o pyproject now ignores current directory when looking up pypa/build. This fixes
  compatibility with packages that have build.py in root directory but breaks cache
  keys.

===============================
bst-plugins-experimental 1.95.4
===============================
o pypi source now blocks prereleases by default and has config option to enable them

===============================
bst-plugins-experimental 1.95.3
===============================
o Fixed inclusions and exclusions for pypi source
  These were broken during refactoring

===============================
bst-plugins-experimental 1.95.2
===============================
o Make pypi source plugin ignore versions which fail to validate

===============================
bst-plugins-experimental 1.95.1
===============================
o This release is done only to fix publishing system

===============================
bst-plugins-experimental 1.95.0
===============================
o git_tag fixed to work correctly when sources are stored under git directory
o Updated to test against bst 1.95.4
o pypi source switched to use fnmatch rather than regexp matching for versions
o Fixed ostree source plugin tracking not to have extraneous whitespace
o cargo plugin moved to buildstream-plugins repository
o Added pyproject element which makes using pypa/build and pypa/installer easier
o Duplicated plugins have been removed from this repository in favour of
  buildstream-plugins or BuildStream core.
o flatpak_repo element supports multiple branches
o End-to-end tested to work with latest BuildStream 2 API changes

===============================
bst-plugins-experimental 1.93.8
===============================
o patch_queue added to project
o cpan, pypi and check_forbidden added to project.conf where missing
o this repo now uses only git_tag for testing instead of git plugin

===============================
bst-plugins-experimental 1.93.7
===============================
o Fix quilt to use QUILT_PATCHES instead of cwd
o cpan source plugin added to project
o pypi source plugin added to project
o ostree mirror parameter removed
o cargo plugin advertises it's a BuildStream component through User-Agent
o snap_image build element added to project
o zip plugin added to project
o A lot of API fixes and test improvements

===============================
bst-plugins-experimental 1.93.6
===============================
o Fix issues in git_tag plugin
o Expose new git_module plugin
o Added project.conf so that plugins can be loaded through junctions

===============================
bst-plugins-experimental 1.93.5
===============================
o Adapt to API changes in BuildStream
o Fixes to OCI Healthcheck and StopSignal handling
o Remove fix-python-timestamps from distutils and pip plugins
o Make cargo depend on toml instead of pytoml
o git-tag supports relative (./foo.git) URL's for submodules
o make has make-args and make-install-args for easier customization
o makemaker.yaml: use DESTDIR to set the install root
o cmake now uses ninja by default

===============================
bst-plugins-experimental 1.93.4
===============================

o OSTree plugin now doesn't need PyGobject anymore. The 'extras'
  target 'ostree' has been dropped. You can safely remove it.

===============================
bst-plugins-experimental 1.93.3
===============================

o BREAKING CHANGE: Adapt plugins to BuildStream 1.93.3.

  Due to backwards incompatible nature of the API changes in BuildStream, this
  release also drops support for BuildStream <= 1.93.3.

o Add bazelize element plugin.

===============================
bst-plugins-experimental 1.93.1
===============================

o Use `buildstream.exceptions` module available in BuildStream 1.93.1.
o Add pip source plugin
o Add pip build element plugin

===============================
bst-plugins-experimental 1.93.0
===============================

o bst-plugins-experimental has been adapted to work with BuildStream 1.93.0.
  Since the new BuildStream update comes with some breaking changes, this
  package no longer supports BuildStream 1.93.*.

===================
bst-external 0.15.0
===================

o Adapt to new BuildStream plugin namespace in a non-breaking way.

===================
bst-external 0.14.0
===================

o Add deb source plugin
o Add tar source plugin

===================
bst-external 0.13.0
===================

o Add ostree source plugin
o Forked bst-plugins-experimental from bst-external at 3d52546
o Removed docker source, it's in bst-plugins-container now

===================
bst-external 0.12.0
===================

o collect_manifest.py: Uses its own of getting dependencies instead of relying on private APIs.
o git_tag.py: Fixed the config field "track-extra" not being supported

===================
bst-external 0.11.0
===================

o git_tag: Allow pattern matching of tags and tracking multiple branches.
o git_tag: Don't download unused submodules

===================
bst-external 0.10.0
===================

  o Add quilt source plugin for applying patch series.

==================
bst-external 0.9.0
==================

  o Add the ability to generate manifest files using the collect_manifest plugin.

==================
bst-external 0.8.0
==================

  o flatpak_image plugin now automatically moves all files in /etc into
    /files/etc
  o git_tag: Fixed tracking not picking up new changes if the ref is already
    set
  o Add flatpak_repo element plugin for creating a flatpak repository from
    flatpak_image elements.

==================
bst-external 0.7.1
==================

  o Fix git_tag plugin not updating tags if the source fetches from multiple
    repositories

==================
bst-external 0.7.0
==================

  o Add tar_element plugin for creating tarballs
  o Make git_tag plugin resilient to changes in buildstream's built-in git plugin.

==================
bst-external 0.6.2
==================

  o Add support for non-annotated tags in git_tag.
  o Make git_tag plugin fall back to normal behaviour if no tags are found.

==================
bst-external 0.6.1
==================

  o Add entrypoint for git_tag plugin

================
bst-external 0.6
================

  o New git-tag plugin: An extension to the BuildStream git plugin
    which allows automatic tracking of the latest tag in a branch.
  o fastboot_bootimg Element - Uses artifacts generated by buildstream to produce an
    image that can be flashed to the boot partition using fastboot.
  o fastboot_ext4 Element - Uses artifacts generated by buildstream to produce an
    ext4sparse image that can be flashed to userdata partitions or others using fastboot.

==================
bst-external 0.5.1
==================

  o collect-integration: Fix the collect-integration plugin not being loadable
    via pip

================
bst-external 0.5
================

  o flatpak-image: Preserve case-sensitivity of flatpak metadata.

================
bst-external 0.4
================

  o Introducing James Ennis as co-maintainer
  o dpkg-build: The debian/rules file no longer has to be made executable
  o Installation no longer erroneously installs the tests
  o README.md includes documentation for how to use and install it
  o The test suite can now be run in versions of buildstream that forbid paths
    leading outside the project.
  o The Collect Integration Element has been imported from the freedesktop-sdk
    project. This plugin collects the integration commands of all its
    dependencies into a single shell script.


==================
bst-external 0.3.1
==================

  o Updated the version in setup.py for the latest version, and updated the
    release process so this is less likely to happen again.

================
bst-external 0.3
================

  o The FlatPak Image Element now creates directories for each extension
    defined in the metadata when built.

================
bst-external 0.2
================

  o `url` key of `docker` source plugin is now deprecated. `registry-url`
    and `image` keys should be used instead. Users of the plugin should
    either:
    - update their element definitions (recommended), or
    - continue using an older version of bst-external.

================
bst-external 0.1
================

First release of bst-external. API is not considered stable yet.
Plugins added in this release:

  o Docker Source - Pulls Docker images from Docker registries.
  o DPKG Build Element - Builds elements using Debian sources.
  o DPKG Deploy Element - Uses artifacts generated by buildstream to produce
    Debian packages.
  o FlatPak Image Element - Uses artifacts generated by buildstream to produce
    FlatPak images.
  o Quilt Source - Runs Quilt on top of previously-staged sources.
  o X86Image Element - Uses artifacts generated by buildstream to produce a
    disk image.
